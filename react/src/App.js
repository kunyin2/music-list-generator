import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import UserHome from './pages/UserHome/UserHome';
import Header from './Components/Header/Header';
import Login from './pages/Login/Login';
import logo from './logo2.png';
import Register from './pages/Register/Register';
import UserUpdate from './pages/UpdateProfile/UserUpdate';
import CreatePlaylist from './pages/CreatePlaylist/CreatePlaylist';
import OthersPlaylist from './pages/OthersPlaylist/OthersPlaylist';
import Myplaylist from './pages/Myplaylist/Myplaylist';
import SearchSong from './pages/SearchSongs/SearchSongs';
import './App.css';

const PrivateRoute = ({component: Component, isAuthenticated: isAuthenticated, ...rest }) => (
  <Route {...rest} render={(props) => (
    isAuthenticated === true
    ? <Component {...props} />
    : <Redirect to='/login' />
  )}/>
)
class App extends React.Component{
  state={
    isAuthenticated:false,
    email:'',
    username: '',
  };
  updateUserProfile = (email, username) => {
    this.setState({ email: email });
    this.setState({ username: username });
  };
  handleLogin = (isAuthenticated) => this.setState({isAuthenticated})
  render(){
    const {isAuthenticated} = this.state;
    return (
      <div>
        <Header isLogged={this.handleLogin} isAuthenticated={isAuthenticated} username={this.state.username} />
        <Switch>
        <Route exact path='/' render={(props) =>
             <UserHome {...props} updateUserProfile={this.updateUserProfile} />
                    }/>
        <PrivateRoute
            isAuthenticated={isAuthenticated}
            path="/playlist"
            component={(props) => (<CreatePlaylist email={this.state.email} username={this.state.username} {...props} />)}
          />
        <PrivateRoute
            isAuthenticated={isAuthenticated}
            path="/userupdate"
            component={(props) => (<UserUpdate email={this.state.email} username={this.state.username} {...props} />)}
          />
         <PrivateRoute
               isAuthenticated={isAuthenticated}
                path="/myplaylist"
                component={(props) => (<Myplaylist email={this.state.email} username={this.state.username} {...props} />)}
          />
        <Route exact path='/login' render={(props) =>
            <Login {...props} isLogin={this.handleLogin}/>
            } />
        <Route exact path='/register' render={(props) =>
            <Register {...props} isLogin={this.handleLogin}/>
            }/>
        <Route exact path='/searchsong' render={(props) =>
            <SearchSong {...props} />
            }/>
        <Route exact path='/othersplaylist' render={(props) =>
                    <OthersPlaylist {...props} />
                    }/>
        </Switch>
        
      </div>

    )
  };
}
export default App;
