import React, { useDebugValue } from 'react'
import Logo from '../../assets/logoSmall.png'
import './Register.css'

class Register extends React.Component{
    state={
        username:'',
        email:'',
        pwd:'',
        registerStatus:'',
    };
    hanldeChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    };
    handleSubmit = (e)=>{
        e.preventDefault();
        //insert data to db
        const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ username: this.state.username,
                                       email: this.state.email,
                                       pwd: this.state.pwd})
            };
        fetch('/api/register', requestOptions)
                .then(response => response.json())
                .then(data => {
                    this.setState({registerStatus: data.status});
                    if (this.state.registerStatus === 'successful'){
                         this.props.isLogin(true)
                          this.props.history.push({
                               pathname: '/',
                                 state: { email: this.state.email, username: this.state.username }
                             });
                         }else{
                                  alert("Oops, email has already existed, please log in")
                            }
                });
    }
    render(){
        return(
            <div className='div-login'>
                <div>
                    {/* <Logo/> */}
                   <img src={Logo} className='div-login-logo' alt="Logo" />
                </div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input type='text' name='username' placeholder='username' required onChange={this.hanldeChange}/>
                        <input type='email' name='email' placeholder='email' required onChange={this.hanldeChange}/>
                        <input type='password' name='pwd' placeholder='password' required onChange={this.hanldeChange}/>
                        <button> Register </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Register;