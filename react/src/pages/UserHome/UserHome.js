import React from 'react';
import recordLogo from '../../assets/logoHalfRecord.png';
import logo from '../../assets/logo2.png';
import './UserHome.css'
// const UserHome = () => <h1> UserHome </h1>
class UserHome extends React.Component{
     state={
        email:'',
        username: '',
      };
    constructor(props) {
        super(props);
        if(this.props.location.state !== undefined) {
            console.log("inside constructor");
            this.state = {
                email: this.props.location.state.email,
                username: this.props.location.state.username,
            };
            // call parent function
            this.props.updateUserProfile(this.props.location.state.email, this.props.location.state.username);
        }
    }
    render(){
        return (
            <div>
                <div className="App">
                    <div className="App-body" >
                        <img src={recordLogo} className="App-logo" alt="logo" />
                        <h1 style={{ color: '#6bcf71' }}>
                            Your Day. <br/> In Music.
                        </h1>
                        <a
                            className="App-link"
                            href="http://172.22.152.15/"
                            target="_blank"
                            rel="noopener noreferrer"
                            style={{  textDecoration: 'none', color: '#6bcf71' }}
                        >
                        Tell us what you’re up to and we will design a playlist to match your schedule​
                        </a>
                        <div style= {{width:'100%'}}>
                           <button className="playlist" onClick={()=> this.props.history.push('/othersplaylist') }>See Others' Playlists</button>
                           <button className="playlist" onClick={()=>this.props.history.push('/playlist')}>Create Your Own</button>
                           <button className="playlist" onClick={()=> this.props.history.push('/myplaylist') }>My Playlists</button>
                        </div>
                        <div style= {{width:'100%'}}>
                              {/* <button className="playlist"onClick={()=>this.props.history.push('/searchsong')}>search</button>
                              <button className="playlist">Placeholder</button> */}
                         </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default UserHome;