import React from 'react';
import Collapsible from 'react-collapsible';
import './Myplaylist.css';
import Playlist from '../Playlist/DragPlaylist';
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import Popup from '../../Components/Popup/Popup';
import PopupUpdatePlaylistName from '../../Components/Popup/PopupUpdatePlaylistName';

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  console.log(result)
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 0 ${grid}px 0`,
  width: 600,
  // change background colour if dragging
  background: isDragging ? "lightgreen" : "lightblue",

  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightgrey" : "#2941ab",
  padding: grid,
  position: 'absolute', left: '30%', top: '40%',
  width: 630,
});

const  updateSongOrderInsidePlaylist = (playlistID, reorderedSongs, activity, actionType)=> {
        console.log('update songs order');
        console.log(activity);
        //if(activity!=""){ //If something was in the activity string
        const requestOptions = {
          method: 'PUT',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify({
          playlistID: playlistID,
          reorderedSongs: reorderedSongs,
          actionType: actionType,
          activity: activity,
           })
        };

        fetch('/api/playlist/mine', requestOptions)
              .then(response => response.json())
              .then(data => {
                    console.log(data.status)
              });
        //} 
        

  };

class Myplaylist extends React.Component{
  state = {
          myplaylist: [],
          showPopup: false,
          clickedPlaylist: 0,
          playlistName: "",
          showEditPopup: false,
          clickedEditIndex: 0,
      };
  constructor(props) {
    super(props);
    this.onDragEnd = this.onDragEnd.bind(this);
  };
  searchPlaylist = () =>{
      const requestOptions = {
                             method: 'POST',
                             headers: { 'Content-Type': 'application/json' },
                             body: JSON.stringify({ email: this.props.email,
                                                    keywords: this.state.playlistName
                            })
                         };
              fetch('/api/playlist/mine', requestOptions)
                            .then(response => response.json())
                            .then(data => {
                                 console.log(data.myplaylist)

                                 this.setState({ myplaylist: data.myplaylist })
                                 if(this.state.myplaylist.length === 0){
                                     alert("Oops, no playlist matches your search~")
                                  }
                            });
  }
  handleChange = ({ target }) => {
            this.setState({ [target.name]: target.value });
         };

  handlePlaylistClick = (arrIndex) => {
          let temp = [...this.state.myplaylist]
          temp[arrIndex]['displayOnPage'] = !temp[arrIndex]['displayOnPage']
          this.setState({ temp })
  };

  addSongToPlaylist = (song,activity) => {
     console.log('song: '+ song +" activity: " + activity);
     let playlistID = (this.state.clickedPlaylist).toString()

     let newItems = [...this.state.myplaylist];
     console.log("new item", newItems)

     let maxSongNum = 0;
     let tempSongs = []
     newItems = newItems.map(item => {
               if (item.id === playlistID) {
                  tempSongs = item.songs;
                  // check if songs array is empty
                  if (tempSongs === undefined || tempSongs.length == 0) {
                      // array empty or does not exist
                      maxSongNum = 1
                  } else {
                    const maxSong = tempSongs.reduce((prev, current) => (prev.songNumber > current.songNumber) ? prev : current)
                    maxSongNum = maxSong.songNumber + 1
                  }
                  song['songNumber'] = maxSongNum
                  tempSongs.push(song);
                  item.songs = tempSongs;
               }
               return item;
             });
     this.setState({
               myplaylist: newItems
             });
    updateSongOrderInsidePlaylist(playlistID, tempSongs, activity, "addnew");
    //console.log("updateSongOrderInsidePlaylist just called: " + playlistID + activity)
    // close pop-up
    this.addSongPopup(-1, false)
  }

  componentDidMount() {
        const requestOptions = {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({ email: this.props.email,
                                                keywords: this.state.playlistName
                       })
                    };
         fetch('/api/playlist/mine', requestOptions)
                       .then(response => response.json())
                       .then(data => {
                            console.log(data.myplaylist)
                            this.setState({ myplaylist: data.myplaylist })
                       });
    };
   displayObject = (condition) => {
        if(condition) {
            return {}
        } else {
            return {display: 'none'}
        }
   };

   editPlaylist = (index, change)=>{
       this.setState({
                  showEditPopup: !this.state.showEditPopup,
              });
       if(change) {
             this.setState({
                 clickedEditIndex: index,
                 });
       }
   };
   updatePlaylistName = (updateName) => {
        // get old name
        if(this.state.clickedEditIndex >= 0) {
            let oldPlaylist = this.state.myplaylist[this.state.clickedEditIndex];
            console.log(oldPlaylist.playlistName);
            if(updateName && updateName !== oldPlaylist.playlistName) {
                console.log("send update to python")
                // send data to python
                const requestOptions = {
                                                     method: 'PUT',
                                                     headers: { 'Content-Type': 'application/json' },
                                                     body: JSON.stringify({
                                                     playlistName: updateName,
                                                     playlistID: oldPlaylist.playlistID,
                                                     email: this.props.email

                                                    })
                         };
                       fetch('/api/updatePlaylistName', requestOptions)
                                                .then(response => response.json())
                                                .then(data => {
                                                     console.log(data.status)
                                                });

                // update state.myplaylist
                let newItems = [...this.state.myplaylist];
                console.log(newItems);

                newItems[this.state.clickedEditIndex].playlistName = updateName;

                this.setState({
                              myplaylist: newItems
                     });
            } else {
                console.log("new name is empty or same.")
            }
        }
        this.editPlaylist(-1, false);
   };
   deletePlaylist = (playlistIndex)=>{
      const requestOptions = {
                                       method: 'DELETE',
                                       headers: { 'Content-Type': 'application/json' },
                                       body: JSON.stringify({
                                       email: this.props.email,
                                       playlistID: this.state.myplaylist[playlistIndex].playlistID,
                                      })
                                   };
                         // Simple GET request using fetch
               fetch('/api/myplaylist', requestOptions)
                                      .then(response => response.json())
                                      .then(data => {
                                           console.log(data.status)
                                      });

               this.setState((prevState) => {
                         let temp = {
                             myplaylist: [...prevState.myplaylist]
                         }
                         temp.myplaylist.splice(playlistIndex, 1);
                         return temp
                       });
                     };

   deleteSong = (songIndex, playlistIndex) => {

         const requestOptions = {
                                 method: 'DELETE',
                                 headers: { 'Content-Type': 'application/json' },
                                 body: JSON.stringify({
                                 email: this.props.email,
                                 playlistID: this.state.myplaylist[playlistIndex].playlistID,
                                 songID : this.state.myplaylist[playlistIndex].songs[songIndex].songID

                                })
                             };
                   // Simple GET request using fetch
         fetch('/api/playlist/song', requestOptions)
                                .then(response => response.json())
                                .then(data => {
                                     console.log(data.status)
                                });

        this.setState((prevState) => {
          let temp = {
              myplaylist: [...prevState.myplaylist]
          }
          temp.myplaylist[playlistIndex].songs.splice(songIndex, 1);
          return temp
        });
      };

    addSongPopup = (playlistIndex, change) => {
        this.setState({
            showPopup: !this.state.showPopup,
        })
        if(change) {
            this.setState({
                    clickedPlaylist: playlistIndex,
                })
        }

    };
  onDragEnd(result) {
    // dropped outside the list
    console.log("innner drag");
    if (!result.destination) {
      return;
    }
    const sourceIndex = result.source.index;
    const destIndex = result.destination.index;
    if (result.type === "droppableItem") {
      const myplaylist = reorder(this.state.myplaylist, sourceIndex, destIndex);
      this.setState({
        myplaylist
      });
    } else if (result.type === "droppableSubItem") {
      const itemSubItemMap = this.state.myplaylist.reduce((acc, item) => {
        acc[item.id] = item.songs;
        return acc;
      }, {});

      const sourceParentId = (result.source.droppableId);
      console.log("source parent id:", sourceParentId)
      const destParentId = (result.destination.droppableId);

      const sourceSubItems = itemSubItemMap[sourceParentId];
      console.log("source sub-items: ", sourceSubItems);
      const destSubItems = itemSubItemMap[destParentId];
      let newItems = [...this.state.myplaylist];

      /** In this case subItems are reOrdered inside same Parent */
      if (sourceParentId === destParentId) {
        const reorderedSubItems = reorder(
          sourceSubItems,
          sourceIndex,
          destIndex
        );
        newItems = newItems.map(item => {
          if (item.id === sourceParentId) {
            item.songs = reorderedSubItems;
          }
          return item;
        });
        this.setState({
          myplaylist: newItems
        });
        var activity = ""//In order to keep consistent, pass an empty activity
        updateSongOrderInsidePlaylist(sourceParentId, reorderedSubItems, activity, "dragdrop");

      } else {
      // drag drop from different parents
         return;
      }
    }
  };

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
    <div>
    <input type="text" name="playlistName" placeholder="search your playlist" className="searchInput" onChange={this.handleChange}/>
    <button onClick={this.searchPlaylist}  className="searchButton123">Search</button>

      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable" type="droppableItem">
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
            >
              {this.state.myplaylist.map((playlistData, index) => (
                <Draggable key={playlistData.id} draggableId={playlistData.id} index={index}>
                  {(provided, snapshot) => (
                    <div>
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        style={getItemStyle(
                          snapshot.isDragging,
                          provided.draggableProps.style
                        )}
                      >
                       <div>
                         <div
                                                        onClick={()=> this.handlePlaylistClick(index)}
                                                        {...provided.dragHandleProps} >
                                                              {playlistData.playlistName}


                         </div>
                        <button onClick={()=> this.editPlaylist(index, true)} className="edit-playlist-button">Edit</button>
                        <button onClick={()=> this.deletePlaylist(index)} className="delete-playlist-button">Delete</button>
                        </div>
                        <div style={this.displayObject(playlistData['displayOnPage'])}>
                                  <Playlist
                                                        subItems={playlistData.songs}
                                                        type={playlistData.id}
                                                        key={playlistData.playlistID}
                                                        playlistIndex={index}
                                                        playlistName={playlistData.playlistName}
                                                        dateCreated={playlistData.dateCreated}
                                                        email={playlistData.email}
                                                        scheduleID={playlistData.scheduleID}
                                                        songs={playlistData.songs}
                                                        deleteSong={this.deleteSong}
                                  />
                              <button onClick={()=> this.addSongPopup(playlistData.id, true)} className="addSongButton">Add Song</button>

                        </div>
                      </div>
                      {provided.placeholder}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
             <div>
                    { this.state.showPopup ?
                        <Popup
                            text='input some keywords to search songs'
                            closePopup={this.addSongPopup}
                            addSongToPlaylist={this.addSongToPlaylist}
                            clickedPlaylistID={this.state.clickedPlaylist}
                        />

                        : null
                    }
              </div>
              <div>
                     { this.state.showEditPopup ?
                                      <PopupUpdatePlaylistName
                                          text='enter playlist name'
                                          closePopup={this.editPlaylist}
                                          playlistName={this.state.myplaylist[this.state.clickedEditIndex].playlistName}
                                          updatePlaylistName={this.updatePlaylistName}

                                      />

                                      : null
                                  }
                            </div>
      </DragDropContext>
</div>
    );
  }
}

export default Myplaylist;