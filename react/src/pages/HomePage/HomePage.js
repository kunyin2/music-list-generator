import React from 'react';
import logo from '../../assets/logo2.png';
import { Switch, Route } from 'react-router';
import UserHome from '../UserHome/UserHome';
import Header from '../../Components/Header/Header';
import CreatePlaylist from '../CreatePlaylist/CreatePlaylist';
import './HomePage.css'
import UserUpdate from '../UpdateProfile/UserUpdate';

const HomePage = ({handleLogged}) =>{
    return(
        <div>
            <Header isLogged={handleLogged}/>
            <Switch>
                <Route exact path='/' component={UserHome}/>
                <Route exact path='/playlist' component={CreatePlaylist}/>
                <Route exact path='/userupdate' component={UserUpdate}/>
            </Switch>
        </div>
    )
}

export default HomePage;