import React from 'react'
import './Playlist.css'
import Song from '../Song/Song'

class Playlist extends React.Component {

    deleteSongFromPlaylist = songIndex => {
        return this.props.deleteSong(songIndex, this.props.playlistIndex);
    };

    render() {
        return(
            <div className="playlist-body">
                <p className="playlist-name">Playlist Name: {this.props.playlistName}</p>
                <div className="song-list">
                    <table id="playlistTable">
                        <tbody> {/*A comment to say apparently not having tbody sometimes causes weird bugs on page reload */}
                            <tr>
                                <th>Song Name</th>
                                <th>Duration</th>
                                <th>Artist</th>
                                <th>Year</th>
                            </tr>
                            {this.props.songs.map((songData, index) => (
                                          <Song
                                                           key={songData.songID}
                                                           songID={songData.songID}
                                                           songIndex={index}
                                                           songName={songData.songName}
                                                           artist={songData.artist}
                                                           duration={songData.duration}
                                                           popularity={songData.popularity}
                                                           year={songData.year}
                                                           deleteSongFromPlaylist={this.deleteSongFromPlaylist}
                                          />
                                                    ))}
                        </tbody>
                    </table>

                </div>

            </div>
        )
    }
}

export default Playlist;
