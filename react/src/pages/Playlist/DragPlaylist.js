import React from 'react'
import './DragPlaylist.css'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd"
import Song from '../Song/DragSong'

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  padding: grid * 2,
  margin: `0 10px 10px 0`,

  display: "inline-flex",
  width: "580px",
  padding: "10px",

  // change background colour if dragging
  background: isDragging ? "lightgreen" : "grey",
  display: "inline-flex",
  padding: "10px",
  margin: "0 10px 10px 0",
  border: "1px solid grey",
  // styles we need to apply on draggables
  ...draggableStyle
});

const getListStyle = isDraggingOver => ({
  background: isDraggingOver ? "lightblue" : "lightgrey",
  padding: grid,
  margin: "10px 0"
});



class Playlist extends React.Component {
    constructor(props) {
        super(props);

    };
 deleteSongFromPlaylist = songIndex => {
        return this.props.deleteSong(songIndex, this.props.playlistIndex);
    };

    render() {
        return (
              <Droppable droppableId={this.props.type} type={`droppableSubItem`}>
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    style={getListStyle(snapshot.isDraggingOver)}
                  >
                    {this.props.subItems.map((songData, index) => (
                      <Draggable key={songData.id} draggableId={songData.id} index={index}>
                        {(provided, snapshot) => (
                          <div style={{ display: "flex" }}>
                            <div
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              style={getItemStyle(
                                snapshot.isDragging,
                                provided.draggableProps.style
                              )}
                            >
                              <div
                                {...provided.dragHandleProps}
                              >
                                          <Song
                                                key={songData.songID}
                                                songID={songData.songID}
                                                songIndex={index}
                                                songName={songData.songName}
                                                artist={songData.artist}
                                                year={songData.year}
                                                duration={songData.duration}
                                                activity = {songData.activity}
                                                deleteSongFromPlaylist={this.deleteSongFromPlaylist}

                                          />
                              </div>
                            </div>
                            {provided.placeholder}
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            );
    }
}

export default Playlist;