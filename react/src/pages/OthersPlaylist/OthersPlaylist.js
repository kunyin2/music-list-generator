import React from 'react'
import './OthersPlaylist.css'
import PlaylistPaginate from './PlaylistPaginate'
import Pagination from './Pagination'
import Playlist from '../Playlist/Playlist'

class OthersPlaylist extends React.Component{
    state = {
        otherPlaylist: [],
        loading: false,
        currentPage: 1,
        playlistPerpage: 5,
    };

    // chage page
    paginate = (pageNumber, totalPages) => {
        console.log('change page number: ', pageNumber);
        if(pageNumber > 0 && pageNumber <= totalPages ) {
            this.setState({
                    currentPage: pageNumber,
                })
        } else {
            console.log('no change.')
        }

    }
    componentDidMount() {
          // Simple GET request using fetch
         this.setState({
            loading: true,
         })
         fetch('/api/playlist/other')
                       .then(response => response.json())
                       .then(data => {
                            console.log(data.otherPlaylist)
                            this.setState({ otherPlaylist: data.otherPlaylist, loading: false })
                       });
    }

    render(){
       let unloginMode = {}
       let loginMode = {}
       if(this.props.username !== "") {
           unloginMode.display = "none"
       } else {
           loginMode.display = "none"
       }
       const indexOfLastList = this.state.currentPage * this.state.playlistPerpage;
       const indexOfFirstList = indexOfLastList - this.state.playlistPerpage;
       const currentLists = this.state.otherPlaylist.slice(indexOfFirstList, indexOfLastList);

       return(
            <div>
                <Pagination
                                playlistPerpage={this.state.playlistPerpage}
                                totalList={this.state.otherPlaylist.length}
                                paginate={this.paginate}
                                currentPage={this.state.currentPage}
                            />
                <PlaylistPaginate playlists={currentLists} loading={this.state.loading} />
            </div>
       )
    }
}

export default OthersPlaylist;


