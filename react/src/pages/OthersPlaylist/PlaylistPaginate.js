import React from 'react'
import Playlist from '../Playlist/Playlist'

const PlaylistPaginate = ({ playlists, loading }) => {
    if(loading) {
        return <h2>Loading ...</h2>;
    }
    return (
        <div>
                {playlists.map(playlistData => (
                              <Playlist
                                   key={playlistData.playlistID}
                                   playlistName={playlistData.playlistName}
                                   dateCreated={playlistData.dateCreated}
                                   email={playlistData.email}
                                   scheduleID={playlistData.scheduleID}
                                   songs={playlistData.songs}
                                   />
                           ))}
        </div>
    )
};

export default PlaylistPaginate;