import React from 'react'
import './Pagination.css'

const Pagination = ({ playlistPerpage, totalList, paginate, currentPage }) => {
    const pageNumbers = [];
    for(let i = 1; i <= Math.ceil(totalList / playlistPerpage); i++) {
        pageNumbers.push(i);
    }

    return (
        <div className="pagination">
          <a href="#" onClick={() => paginate(currentPage - 1, pageNumbers.length)}>&laquo;</a>
           {pageNumbers.map(number => (
                            <a key={number} href="#"
                                onClick={() => paginate(number, pageNumbers.length)}
                                className={number === currentPage ? 'active':''}>
                              {number}
                            </a>
           ))}
          <a href="#" onClick={() => paginate(currentPage + 1, pageNumbers.length)}>&raquo;</a>
        </div>
    )
}
export default Pagination;