import React from 'react'
import './Song.css'

class Song extends React.Component {

    render() {
        return(
            <tr>
                <td>{this.props.songName}</td>
                <td>{this.props.duration}</td>
                <td>{this.props.artist}</td>
                <td>{this.props.year}</td>

            </tr>
        )
    }
}

export default Song;
