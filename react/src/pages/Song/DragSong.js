import React from 'react'
import './DragSong.css'


class Song extends React.Component {

    render() {
        return(


        <table id="playlistTable">
        <div className="song">


             <table id="playlistTable">
                                                      <thead>
                                                          <tr>
                                                              <th>Name</th>
                                                              <th>Artist</th>
                                                              <th>Duration</th>
                                                              <th>Activity</th>
                                                              <th>Year</th>
                                                              <th>Edit</th>
                                                          </tr>
                                                      </thead>
                                                      <tbody>
                                                           <tr>
                                                                          <th>{this.props.songName}</th>
                                                                          <th>{this.props.artist}</th>
                                                                          <th>{this.props.duration}</th>
                                                                          <th>{this.props.activity}</th>
                                                                          <th>{this.props.year}</th>
                                                                          <th> <button className="deleteButton" onClick={()=> this.props.deleteSongFromPlaylist(this.props.songIndex) } >Delete</button></th>

                                                            </tr>
                                                      </tbody>
                                                  </table>



        </div>
        </table>
        )
    }
}

export default Song;