import React, { useDebugValue } from 'react'
import Logo from '../../assets/logoSmall.png'
import './UserUpdate.css'

class UserUpdate extends React.Component{
    state={
        email:'',
        usernameNEW:'',
        pwdNEW:'',
    };
    constructor(props) {
        super(props);
        this.state = {
               email: this.props.email,
               usernameNEW: this.props.username,
        };
    };

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    };
    handleSubmit = (e)=>{
        e.preventDefault();
        //insert data to db
        const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ username: this.state.usernameNEW,
                                       email: this.state.email,
                                       pwd: this.state.pwdNEW})
            };
        fetch('/api/userupdate', requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.status === 'successful'){
                          this.props.history.push({
                               pathname: '/',
                                 state: { email: this.state.email, username: this.state.usernameNEW }
                             });
                         }else{
                                 this.props.history.push({
                                      pathname: '/login',
                                      state: { username: this.state.username }
                                  });
                            }
                });
    }
    render(){
        return(
            <div className='div-login'>
                <div>
                    {/* <Logo/> */}
                   <img src={Logo} className='div-login-logo' alt="Logo" />
                </div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input type='email' name='email' value={this.props.email}  readOnly />
                        <input type='text' name='usernameNEW' value={this.state.usernameNEW}  required onChange={this.onChange}/>
                        <input type='password' name='pwdNEW' placeholder='password' required onChange={this.onChange}/>
                        <button> Update </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default UserUpdate;