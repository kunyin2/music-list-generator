import React, { useDebugValue } from 'react'
import Logo from '../../assets/logoSmall.png'
import './Login.css'

class Login extends React.Component{
    state={
        email:'',
        pwd:'',
        username:'',
    }
    handleChange=(e)=>{
         this.setState({
                    [e.target.name]: e.target.value,
                })
    }
    handleSubmit = (e) =>{
        e.preventDefault()
        const requestOptions = {
                        method: 'POST',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({ email: this.state.email,
                                               pwd: this.state.pwd})
                    };
                    console.log(this.state.email);
        fetch('/api/login', requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            if(data.username !== undefined){                                
                                this.props.isLogin(true)
                                this.setState({
                                                  username: data.username,
                                                   email: data.email,
                                          });
                                this.props.history.push({
                                                           pathname: '/',
                                                           state: { email: this.state.email, username: this.state.username}
                                                       });
                            }else{
                               alert(data.status)
                            }

                        });
    };
    render(){
        return(
            <div className='div-login'>
                <div>
                   <img src={Logo} className='div-login-logo' alt="Logo" />
                </div>
                <div>
                    <form onSubmit={this.handleSubmit}>
                        <input type='email' name='email' placeholder='email' required onChange={this.handleChange}/>
                        <input type='password' name='pwd' placeholder='password' required onChange={this.handleChange}/>
                        <button> Login </button>
                        <button onClick={()=> this.props.history.push('/register')}> Register </button>
                    </form>
                </div>
            </div>
        )
    }
}

export default Login;