import React from 'react';
import ScheduleForm from '../../Components/ScheduleForm/ScheduleForm';
import Schedules from '../../Components/Schedules/Schedules';
import './CreatePlaylist.css'

class CreatePlaylist extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
               generated: false, 
        };
    };
    handleGenerate = (generated) => this.setState({generated})
    render(){
        const {generated} = this.state;
        return(
            <div className='create-body'>
            {
                generated?
                <div className="view">
                    <p> Playlist has been generated </p>
                    <button className="view-button" onClick={()=> this.props.history.push('/myplaylist') }> view now </button> 
                </div>
                :
                <div >  
                </div>
            }
                <ScheduleForm handleGenerate={this.handleGenerate} email={this.props.email} username={this.props.username}/>
                <Schedules handleGenerate={this.handleGenerate} email={this.props.email} username={this.props.username}/>
            </div>
        )
    }
}
export default CreatePlaylist;