import React from 'react'
import Song from '../Song/Song';
import Playlist from '../Playlist/Playlist'
import './SearchSongs.css'

class SearchSong extends React.Component{
    state = {
        songlist: [],
        songName: "",
        //state here
    };
    
    searchSongName=(firstLetter)=>{
        var fetchString = '/api/searchsong?limit=10&firstLetter='+firstLetter
        fetch(fetchString)   // Simple GET request using fetch
            .then(response => response.json())
            .then(data => {
                console.log(data.songlist)
                this.setState({ songlist: data.songlist })
            });
    }

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
     };


    render(){
       let unloginMode = {}
       let loginMode = {}
       if(this.props.username !== "") {
           unloginMode.display = "none"
       } else {
           loginMode.display = "none"
       }
       
        return(
            <div className="page">
                <input type="text" name="songName" placeholder="search by song name" onChange={this.handleChange}/>
                <button className="searchButton" onClick={this.searchSongName.bind(this,this.state.songName)}>searchSongName</button>

                <div className="song-body">
                    <p className="song-name">Search results for: RESULTS</p>
                    <div className="song-list">
                    <table id="songTable">
                    <tbody>
                        <tr>
                            <th>Song Name</th>
                            <th>Duration</th>
                            <th>Artist</th>
                            <th>Popularity</th>
                            <th>Year</th>
                        </tr>
                        {this.state.songlist.map(songData => (
                            <Song
                                key={songData.songID}
                                songName={songData.songName}
                                artist={songData.artist}
                                year={songData.year}
                                />
                        ))}
                        </tbody>
                    </table>
                    </div>

                </div>
            </div> //end page
        )
    }
}
export default SearchSong;