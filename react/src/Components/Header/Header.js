import React from 'react';
import logo from '../../assets/logo2.png';

import { withRouter } from 'react-router-dom';
import './Header.css'

const Header = ({history,isLogged,isAuthenticated,username}) =>{
    const handleClick=() =>{
        history.push('/')
        isLogged(false)
    }
    return(
        <nav>
            <div className='div-header'>
                <a href='/' className='iconLogo'>
                    <ul className="logo-list">
                        <li><img src={logo} className="App-logo-small" alt="logo" /></li>
                        <li><ol className="list-text">
                            <li className="big-text"><strong>Play Your Day</strong></li>
                            <li className="small-text"><strong>Music for Every Mood</strong></li>
                        </ol></li>
                    </ul>
                </a>
                <div class="clearfix"></div>
                {
                    isAuthenticated?
                    <div className='div-items'>
                    <p className="Welcome-user" > Welcome, {username} </p>
                        <button className="button-header" onClick={()=> history.push('/') }> HomePage</button>                    
                        <button className="button-header" onClick={()=> history.push('/userupdate') }> Update Profile</button>
                        <button className='button-header' onClick={handleClick}> Log Out </button> 
                    </div>
                    :
                    <div className='div-items'>
                        <button className="button-header" onClick={()=> history.push('/') }> HomePage</button>
                        <button className='button-header' onClick={()=> history.push('/login') }> Login </button>    
                    </div>
                }
            </div>
        </nav>
    )
}

export default withRouter(Header);