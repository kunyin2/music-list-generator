import React from 'react';
import NumericInput from 'react-numeric-input';
import './ScheduleForm.css'

class ScheduleForm extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            nameIn: '',
            eventsIn: [ { activity: '',  duration: 15 }],
            email: this.props.email,
            username: this.props.username,
        };
    }

    handleAddEvent = () =>{
        this.setState({
            eventsIn: this.state.eventsIn.concat([{ activity: '', duration: 15}])
        })
    }
    handleDeleteEvent = idx => () =>{
        this.setState({
            eventsIn: this.state.eventsIn.filter((s, sidx) => idx !== sidx)
        })
    }
    handleEventChange = idx => e => {
        const newEvents = this.state.eventsIn.map((eventIn, sidx) => {
            if (idx !== sidx) return eventIn;
            return {...eventIn, activity: e.target.value};
        })
        this.setState({ eventsIn: newEvents});
    }
    handleDurationChange(idx,val){
        const newEvents = this.state.eventsIn.map((eventIn, sidx) => {
            if (idx !== sidx) return eventIn;
            return {...eventIn, duration: val};
        })
        this.setState({ eventsIn: newEvents});
    }
    handleChange=(e)=>{
        this.setState({
                   [e.target.name]: e.target.value,
               })
   }
   handleSubmit = (e) =>{
        e.preventDefault()    
        console.log(this.props.username)  
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ name: this.state.nameIn,
                                    createdBy: this.props.username,
                                    events: this.state.eventsIn})
        };
        console.log(requestOptions)
        this.setState({
                nameIn: '',
                eventsIn: [ { activity: '',  duration: 15 }],
        })        
        e.target.reset();
        fetch('/api/addSchedule?userEmail='+this.state.email, requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            this.setState({registerStatus: data.status});
                            if (this.state.registerStatus === 'successful'){
                                console.log("Added Schedule")                                
                                this.props.handleGenerate(true) 
                                window.scrollTo(0, 0)
                            }else{
                                console.log("Failed to add schedule")
                            }
                        });
   }
    render(){
        let addEvent = '';
        if(this.state.eventsIn.length < 8){
            addEvent = <button type='button' className='small' onClick={this.handleAddEvent}> + </button>
        }
        else{
            addEvent = '';
        } 
        return(
            <div className='fonts'>
                <h2> Use a New Schedule </h2>
                <form  className='div-form' onSubmit={this.handleSubmit}>
                    <label>
                        Schedule Name: <input type='text' name='nameIn' onChange={this.handleChange} required/>
                    </label>
                    {this.state.eventsIn.map((eventIn, idx) =>(
                        <div>
                            <label> Activity:
                                <select value={eventIn.activity} onChange={this.handleEventChange(idx)} required>
                                    <option value=''>Select...</option>
                                    <option value='Cleaning'>Cleaning</option>
                                    <option value='Driving'>Driving</option>
                                    <option value='Eating'>Eating</option>
                                    <option value='Relaxing'>Relaxing</option>
                                    <option value='Sleeping'>Sleeping</option>
                                    <option value='Studying'>Studying</option>
                                    <option value='Working'>Working</option>
                                    <option value='Workout'>Workout</option>
                                </select>
                            </label>
                            <label> Duration (min):  
                                <NumericInput className='input-duration' min={15} max={60} step={15} totalWidth={100} iconSize={25} value={eventIn.duration} onChange={ (val) => this.handleDurationChange(idx, val)}/>
                            </label>
                            <button type='button' className='small' onClick={this.handleDeleteEvent(idx)}> - </button>
                        </div>
                    ))}
                    <div>
                        {addEvent}
                    </div>
                    <button type='submit'> Generate Playlist </button>
                </form>
            </div>
        )
    }
}

export default ScheduleForm;