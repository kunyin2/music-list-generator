import React from 'react';
import './Event.css'
class Event extends React.Component{
    render(){
        return(
            <div className='div-event'>
                <div style={{fontWeight: 'bold'}}>{this.props.activity}</div>
                <div>{this.props.duration} mins</div>
            </div>
        )
    }
}

export default Event