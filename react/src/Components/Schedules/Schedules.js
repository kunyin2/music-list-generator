import React from 'react';
import Schedule from '../Schedule/Schedule';
import './Schedules.css'

class Schedules extends React.Component{
    state ={
        schedules: [],
        origSchedules: [],
        selected: '',
        scheduleName: '',
        createdBy: '',        
        email:'',
        username:'',
    }
    constructor(props) {
        super(props);
        this.state = {
               createdBy: '', 
               email: this.props.email,
               username: this.props.username,
        };
    };
    async componentDidMount() {
        // Simple GET request using fetch
        const response = await fetch(`/api/availableSchedules`)
        const origSchedules = await response.json() 
        const temp =  origSchedules
        console.log(origSchedules)
        this.setState({schedules: origSchedules, origSchedules})
   }   

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
    };

    searchSchedule = () => {
        var fetchString = '/api/filteredSchedules?limit=10&scheduleName='+this.state.scheduleName +
                                                '&createdBy='+this.state.createdBy
        fetch(fetchString)   
            .then(response => response.json())
            .then(data => {
                console.log(data)
                this.setState({ schedules: data})
            });
    }
    clearSchedule = () => {
        this.setState({ schedules: this.state.origSchedules, scheduleName: '', createdBy: '',});
    }
    handleGenerate = () => {
        console.log(this.state.selected);
        console.log(this.state.selected._id.$oid);
        console.log(this.props.email);
        // validate selected
        var fetchString = '/api/generatePlaylist?scheduleID='+this.state.selected._id.$oid+'&userEmail='+this.state.email
        console.log(fetchString)
        // post to sent object
        const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ 
                                    username: this.state.username,
                                    events: this.state.selected.events})
        };
        console.log(requestOptions)
        fetch(fetchString, requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            if (data.status === 'successful'){
                                console.log("Added playlist")
                                this.props.handleGenerate(true) 
                                window.scrollTo(0, 0)
                            }else{
                                console.log("Failed to generate playlist")
                            }
                        });
    }
    clickHandler = idx =>(e) =>{
        e.preventDefault() // needed to prevent loop on state
        const cur = this.state.schedules[idx];
        console.log(this.state.schedules[idx]._id.$oid);
        this.setState( {selected: cur});         
    }
    render(){
        const {schedules} = this.state
        return(
            <div>
                <h2> Use an Available Schedule </h2>
                <div className='div-filter'>
                    <input type="text" name="scheduleName" placeholder="schedule name"  value={this.state.scheduleName} onChange={this.handleChange}/>
                    <input type="text" name="createdBy" placeholder="created by"  value={this.state.createdBy} onChange={this.handleChange}/>
                </div>
                <button className="searchButton" onClick={this.searchSchedule}>Filter</button>
                <button className="searchButton" onClick={this.clearSchedule}>Clear</button>
                <div className='div-available'>
                    {
                    schedules && 
                    schedules.map((scheduleData,idx) => (
                        <div onClick={this.clickHandler(idx)}> 
                            <Schedule 
                                key={scheduleData._id}
                                id={scheduleData._id}
                                name={scheduleData.name}
                                createdBy={scheduleData.createdBy}
                                events={scheduleData.events}
                            />
                        </div>
                    ))  
                    }
                </div>
                <button onClick={this.handleGenerate}> Generate Playlist </button>
            </div>

        )
    }
}

export default Schedules;