import React from 'react';
import './PopupUpdatePlaylistName.css';


class PopupUpdatePlaylistName extends React.Component {
  state = {
          editPlaylistName: "",
      };
  constructor(props) {
          super(props);
          this.state = {
                 editPlaylistName: this.props.playlistName,
          };
      };

  handleChange = ({ target }) => {
          this.setState({ [target.name]: target.value });
       };

  render() {
    return (
    <div className='popup-name'>
        <div className='popup-inner-name'>
        <h3 style={{marginLeft: '20px'}}>{this.props.text}</h3>
        <div>
            <input name="editPlaylistName" type="text" className="editPlaylistInput" onChange={this.handleChange} value={this.state.editPlaylistName} />
            <div>
           <button className="submitButton" onClick={()=> this.props.updatePlaylistName(this.state.editPlaylistName)}>Submit</button>
           <button className="closeButton1" onClick={()=> this.props.closePopup(-1, false)}>Close</button>

           </div>
         </div>
        </div>
    </div>
);
}
}

export default PopupUpdatePlaylistName;