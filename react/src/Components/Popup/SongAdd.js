import React from 'react'
import './SongAdd.css'
import AddActivityPopup from './AddActivityPopup';


class SongAdd extends React.Component {
    constructor(props){
        super(props);
    }

    render() {
        return(
            <tr>
                <td>{this.props.songName}</td>
                <td>{this.props.duration}</td>
                <td>{this.props.artist}</td>
                <td>{this.props.popularity}</td>
                <td>{this.props.year}</td>
                <td><button onClick={()=> this.props.activityPopup(this.props.songIndex,true)} className="songAddBtn">Add</button></td>
            </tr>
        )
    }
}

export default SongAdd;