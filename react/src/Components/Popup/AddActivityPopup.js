import React from "react";
import './AddActivityPopup.css';

class AddActivityPopup extends React.Component{
    state = {
        songActivity:"",
        songIndex:"",
    }
    constructor(props){
        super(props);

    }

    handleChange = ({ target }) => {
        this.setState({ [target.name]: target.value });
     };

    handleEventChange = (e) => {
        this.setState({ songActivity:e.target.value});
    };
  
    render(){
    return(
    <div className="popup-box">
      <div className="box">
          <div>Which activity would you like to add this song to?</div>
          <select value={this.state.songActivity} onChange={this.handleEventChange} className='activityDropdown'>
                <option value=''>Pre-Classified Activity</option>
                <option value='cleaning'>Cleaning</option>
                <option value='driving'>Driving</option>
                <option value='eating'>Eating</option>
                <option value='relaxing'>Relaxing</option>
                <option value='sleeping'>Sleeping</option>
                <option value='studying'>Studying</option>
                <option value='working'>Working</option>
                <option value='workout'>Workout</option>
          </select>
          <button className="submitButton" onClick={()=>this.props.updateActivity(this.state.songActivity)}>Submit</button>
          <button className="closeButton" onClick={()=> this.props.closePopup(-1,false)}>Close</button>

      </div>
    </div>
  );
}
}

export default AddActivityPopup;
