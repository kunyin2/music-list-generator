import React from 'react';  
import './Popup.css';
import SongAdd from './SongAdd';
import AddActivityPopup from './AddActivityPopup';

class Popup extends React.Component {
    state = {
          songlist: [],
          songName: "",
          songArtist: "",
          songYear: "",
          songActivity: "",
          searchButtonClicked: false,
          showActivityPopup: false,
          clickedActivityIndex: 0,
          activityToAddTo:"",
      };

    handleChange = ({ target }) => {
          this.setState({ [target.name]: target.value });
       };

    handleEventChange = (e) => {
        this.setState({ songActivity:e.target.value});
    }
    recommendSong =()=>{
        if(this.state.songActivity === "") {
            alert("Please choose one activity!");
        } else {
            console.log(this.props.clickedPlaylistID)
            var fetchString = '/api/recommendsong?limit=10&songActivity='+this.state.songActivity +
                                        '&clickedPlaylistID=' + this.props.clickedPlaylistID
            fetch(fetchString)
                   .then(response => response.json())
                   .then(data => {
                      this.setState({ songlist: data.songlist , searchButtonClicked : true})
            });
        }
    }

    searchSong=()=>{
          var fetchString = '/api/searchsong?limit=10&songName='+this.state.songName +
                                                    '&songArtist='+this.state.songArtist+
                                                    '&songYear='+this.state.songYear+
                                                    '&songActivity='+this.state.songActivity

          console.log(fetchString)
          fetch(fetchString)   // Simple GET request using fetch
              .then(response => response.json())
              .then(data => {
                  console.log(data.songlist)
                  this.setState({ songlist: data.songlist , searchButtonClicked : true})
              });
      };

    getSongDetail=(activity, index)=>{
       let song = this.state.songlist[index];
       console.log(song);
       if(activity !== '') {
            song["activity"] = activity;
       }
       song["id"] = "song-" + song["songID"];
       console.log("activity: " + activity + "index: "+ index)
       this.props.addSongToPlaylist(song, activity);
    };

    activityPopup = (playlistIndex, change) => {
        this.setState({
            showActivityPopup: !this.state.showActivityPopup,
        })
        console.log('activityPopupOn')
        if(change) {
            this.setState({
                    clickedActivityIndex: playlistIndex,
                })
            console.log(this.state.clickedActivityIndex)
        }
    };
    
    updateActivity = (activity) =>{
        this.getSongDetail(activity,this.state.clickedActivityIndex)
    }


  render() {
     let clicked = {}
           if(this.state.searchButtonClicked) {
               clicked = {}
           } else {
               clicked.display = "none"
           }

    return (
    <div className='popup'>
        <div className='popup-inner'>
        <h3 style={{marginLeft: '20px'}}>{this.props.text}</h3>
        <div>
           <input type="text" name="songName" placeholder="song name" className='popupSearch' value={this.state.songName} onChange={this.handleChange}/>
           <input type="text" name="songArtist" placeholder="artist" className='popupSearch' value={this.state.songArtist} onChange={this.handleChange}/>
           <input type="text" name="songYear" placeholder="year" className='popupSearch' value={this.state.songYear} onChange={this.handleChange}/>
           <select value={this.state.songActivity} onChange={this.handleEventChange} className='activityDropdown'>
                <option value=''>Select Activity</option>
                <option value='Cleaning'>Cleaning</option>
                <option value='Driving'>Driving</option>
                <option value='Eating'>Eating</option>
                <option value='Relaxing'>Relaxing</option>
                <option value='Sleeping'>Sleeping</option>
                <option value='Studying'>Studying</option>
                <option value='Working'>Working</option>
                <option value='Workout'>Workout</option>
            </select>

            {console.log(this.state.songActivity)}
           <button className="searchButton" onClick={this.searchSong}>Search</button>
           <button className="recommendButton" onClick={this.recommendSong}>Recommend</button>
           <button className="popupCloseButton" onClick={()=> this.props.closePopup(-1, false)}>Close</button>
           <div>
                {this.state.showActivityPopup ? 
                    <AddActivityPopup
                        closePopup={this.activityPopup}
                        updateActivity={this.updateActivity}
                    />:null
                }
            </div>
           {/*<button className="addActivity" onClick={()=> this.activityPopupOn(0,true)}>addActivity</button>*/}
           
        
         </div>

         <div className="song-list" style={clicked}>
                                      <table id="playlistAddSongTable">
                                          <thead>
                                              <tr>
                                                  <th>Song Name</th>
                                                  <th>Duration</th>
                                                  <th>Artist</th>
                                                  <th>Popularity</th>
                                                  <th>Year</th>
                                                  <th>Edit</th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              {this.state.songlist.map((songData, index) => (
                                                            <SongAdd
                                                                             key={songData.songID}
                                                                             songID={songData.songID}
                                                                             songIndex={index}
                                                                             songActivity={songData.activity}
                                                                             songName={songData.songName}
                                                                             artist={songData.artist}
                                                                             duration={songData.duration}
                                                                             popularity={songData.popularity}
                                                                             year={songData.year}
                                                                             getSongDetail={this.getSongDetail}
                                                                             addToActivity={this.updateActivity}
                                                                             activityPopup={this.activityPopup}

                                                            />
                                            ))}
                                          </tbody>
                                      </table>
                                      

                   </div>

        </div>
    </div>
);  
}  
}  

export default Popup;