import React from 'react';
import Event from '../Event/Event';
import './Schedule.css'

class Schedule extends React.Component {
    render(){
        return(
            <button className="schedule-button div-schedule">
                <div className='div-text'>
                    <div>Name: {this.props.name}</div>
                    <div>Created by: {this.props.createdBy}</div>
                </div>
                {this.props.events.map(eventData => (
                    <Event 
                        key={eventData.activity}
                        activity={eventData.activity}
                        duration={eventData.duration}
                    />))}
            </button>
        )
    }
}


export default Schedule;