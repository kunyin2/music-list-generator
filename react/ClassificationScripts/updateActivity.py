import pymysql
import pandas as pd
from audioFeatures_APIcall import *
from classifySongs2 import *


songDF = mergeSongsAndFilter(fpath)

def get_db_connection():
   try:
      conn = pymysql.connect(
         user="root",
         password="admin",
         host="172.22.152.15",
         port=3306,
         database="playYourDay",
         cursorclass=pymysql.cursors.DictCursor
      )
      return conn
   except pymysql.Error as e:
      print(f"Error connecting to MariaDB Platform: {e}")
      sys.exit(1)

def setSongActivity(songDF):
    conn = get_db_connection()
    cursor = conn.cursor()
    for index, song in songDF.iterrows():
        query = 'update Song SET activity=%s WHERE songID=%s;'
        print(song['songName'])
        values =(song['activity'], song['songID'],)
        cursor.execute(query, values)
    
    conn.commit()
    print('committed')


#Script call commented out to prevent accidental execution
#setSongActivity(songDF)


