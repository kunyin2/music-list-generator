import pandas as pd
import os



songDF = pd.read_excel("C:\\Users\\amend\\Desktop\\ImportData.xlsx",header=0)  #nrows=10,
fpath = 'C:\\Users\\amend\\OneDrive - University of Illinois - Urbana\\Database Systems Final Project\\SpotifyAPICall_Audio_Features\\ClassifiedSongs'

def classifySongs(songDF):
    classifiedSongs = pd.DataFrame(columns=['songID','songName','activity'])
    unclassified = 0
    classified = 0
    for index, s in songDF.iterrows():
        nrg = s['energy']
        inst = s['instrumentalness']
        acous = s['acousticness']
        mode = s['mode']
        val = s['valence']
        
        if(nrg >0.6):
            if(inst <=0.2):
                if(val >=0.6):
                    songDF.loc[index,'activity'] = 'workout'
                else:
                    if(mode >=0.6):
                        songDF.loc[index,'activity'] = 'driving'
                    else:
                        songDF.loc[index,'activity'] = 'cleaning'
        elif(nrg <=0.6):
            if(inst >=0.5):
                if(acous >=0.6):
                    songDF.loc[index,'activity'] = 'working'
                elif(acous <=0.2):
                    pass
                else:
                    songDF.loc[index, 'activity'] = 'relaxing'
            elif(inst <=0.2):
                pass #should be covered in other segment
            else:
                if(mode >= 0.6):
                    songDF.loc[index,'activity'] = 'eating'
                else:
                    if(acous <0.2):
                        pass
                    elif(acous >=0.6):
                        songDF.loc[index,'activity'] = 'sleeping'
                    else:
                        songDF.loc[index, 'activity'] = 'studying'
        else:
            #print("unclassified " + s['songName'])
            unclassified += 1
        
        classifiedSongs = classifiedSongs.append({'songID':songDF.loc[index,'songID'],'songName': songDF.loc[index,'songName'],'activity': songDF.loc[index,'activity']}, ignore_index=True)
        print(classifiedSongs.shape[0])
        print(str(songDF.loc[index, 'songName']) + " " + str(songDF.loc[index, 'activity']))
        if(index % 10000 == 0):
            classifiedSongs.to_excel("classifiedSongs"+str(index)+".xlsx")
            classifiedSongs = classifiedSongs.iloc[0:0]
            print("toExcel")
    
    print("#unclassified: " + str(unclassified)+ " #classified: "+ str(classified))

#classifySongs(songDF)

def getFilesInFolder(fpath):
    filelist=[]
    for path, subdirs, files in os.walk(fpath):
        for file in files:
            if (file.endswith('.xlsx') or file.endswith('.xls') or file.endswith('.XLS')):
                filelist.append(os.path.join(path, file))
    number_of_files=len(filelist)
    return filelist
    


def mergeSongsAndFilter(fpath):
    files = getFilesInFolder(fpath)
    fullDF = pd.read_excel(files[0])

    #Combine all of the dataframes together
    for index,f in enumerate(files): 
        print(index)
        if(index != 0): #first one was used to create the DF so skip it
            newDF = pd.read_excel(f)
            fullDF = fullDF.append(newDF)
    
    #Filter so only return classified ones
    filterDF = fullDF[fullDF['activity'].notnull()]
    cleanDF = filterDF[['songID', 'songName', 'activity']]
    cleanDF.to_excel("cleanedClassification.xlsx")
    return cleanDF

#Script call commented out to prevent accidental execution
#mergeSongsAndFilter(fpath)

    










