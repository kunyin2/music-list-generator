import spotipy
from spotipy.oauth2 import SpotifyOAuth
import os
import pandas as pd
import openpyxl
import time

#Spotipy
os.environ["SPOTIPY_CLIENT_ID"] = 'baf7166211d848e0baebcfa80f18cbb0'
os.environ["SPOTIPY_CLIENT_SECRET"] = 'e5034f05df3f4ef092c0e365516c56a5'
os.environ["SPOTIPY_REDIRECT_URI"]='http://localhost:8080'


def findFeatures(activity):
    scope = "user-library-read"
    spotify = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope))
    #results = sp.search(q='playlist:playlist2')
    #print(results['tracks']['href'])
    playlistNames = []
    playlistURLs = []
    playlistIDs = []
    trackIDs = []
    trackURLs = []
    trackNames = []
    audFeats = pd.DataFrame(columns=['activity','id','duration_ms','danceability','energy','key','loudness','mode','speechiness','acousticness','instrumentalness','liveness','valence','tempo','type','time_signature'])
    #Gather a lists of playlists with a key word in it.
    
    results = spotify.search(q='playlist:' + activity, type='playlist',limit=50)
    items = results['playlists']['items']
    if len(items) > 0:
        for i in items:
            playlist = i
            playlistNames.append(playlist['name'])
            playlistURLs.append(playlist['external_urls']['spotify'])
            playlistIDs.append(playlist['id'])       
    #pull a single playlist
    for pl in playlistIDs:
        singlePlaylist = spotify.playlist(pl)

        #Gather a list of track IDs from one playlist
        for i in range(len(singlePlaylist['tracks']['items'])):
            try:
                trackNames.append(singlePlaylist['tracks']['items'][i]['track']['name'])
                trackIDs.append(singlePlaylist['tracks']['items'][i]['track']['id'])
                trackURLs.append(singlePlaylist['tracks']['items'][i]['track']['external_urls']['spotify'])
            except:
                print("trackID: not added")
            #Gather all audio characteristics of all tracks
        for j,tr in enumerate(trackIDs):
            try:
                oneAudFeat = spotify.audio_features(tr)
                print(tr)
            except:
                print("trackID not found")
            for i in range(len(oneAudFeat)):
                try:
                    audFeats= audFeats.append({
                                'activity':activity,
                                'id': oneAudFeat[i]['id'],
                                'duration_ms': oneAudFeat[i]['duration_ms'],
                                'danceability': oneAudFeat[i]['danceability'],
                                'energy': oneAudFeat[i]['energy'],
                                'key': oneAudFeat[i]['key'],
                                'loudness': oneAudFeat[i]['loudness'],
                                'mode': oneAudFeat[i]['mode'],
                                'speechiness': oneAudFeat[i]['speechiness'],
                                'acousticness':oneAudFeat[i]['acousticness'],
                                'instrumentalness':oneAudFeat[i]['instrumentalness'],
                                'liveness': oneAudFeat[i]['liveness'],
                                'valence': oneAudFeat[i]['valence'],
                                'tempo': oneAudFeat[i]['tempo'],
                                'type': oneAudFeat[i]['type'],
                                'time_signature': oneAudFeat[i]['time_signature']
                                }, ignore_index=True)
                    audFeats.to_excel('C:\\Users\\amend\\Desktop\\'+activity +'.xlsx')
                    time.sleep(1)
                except:
                    print("trackID" + tr + " not found.")
    audFeats.to_excel('C:\\Users\\amend\\Desktop\\'+activity +'.xlsx')

#Script call commented out to prevent accidental execution
#findFeatures('workout')


