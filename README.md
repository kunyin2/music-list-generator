# KASA Final Project 

Team KASA semester project 

Project Demo: https://mediaspace.illinois.edu/media/t/1_xsurqphz

To run locally
```
    cd react
    rm -rf build
    npm install
    npm run build
    cd ..
    pip install -r requirements.txt
    flask run
```
Server deploy steps
```
    cd /kasa_project
    scl enable rh-python36 bash
    python –m venv project_venv
    source project_venv/bin/activate
    pip install -r requirements.txt
    cd react
    rm -rf build
    npm install
    npm run build
    sudo systemctl restart httpd
```
