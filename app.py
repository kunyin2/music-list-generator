# app.py
# This is a "hello world" app sample for flask app. You may have a different file.
from flask import Flask, jsonify, request
import pymysql
import sys
from bson import json_util
import json
import datetime
from pymongo import MongoClient
import binpacking
import math
import time


app = Flask(__name__, static_folder="./react/build", static_url_path='/')

@app.route('/')
def index():
   return app.send_static_file('index.html')

def get_db_connection():
   try:
      conn = pymysql.connect(
         user="root",
         password="admin",
         host="172.22.152.15",
         port=3306,
         database="playYourDay",
         cursorclass=pymysql.cursors.DictCursor
      )
      return conn
   except pymysql.Error as e:
      print(f"Error connecting to MariaDB Platform: {e}")
      sys.exit(1)

def db_connect_test():
   try:
      conn = pymysql.connect(
         user="root",
         password="admin",
         host="172.22.152.15",
         port=3306,
         database="playYourDay"
      )
      # Get Cursor
      cursor = conn.cursor()
      cursor.execute("select * from Lyrics")
      test_list = cursor.fetchall()
      return jsonify({'test': test_list})
   except pymysql.Error as e:
      print(f"Error connecting to MariaDB Platform: {e}")
      sys.exit(1)
   finally:
      if conn:
         cursor.close()
         conn.close()

def get_mongo_client():
   try:
      client = MongoClient(
         host = 'mongodb://172.22.152.15:27017', # <-- IP and port go here
         serverSelectionTimeoutMS = 3000, # 3 second timeout
         username="admin",
         password="admin123",
      )
      return client
   except pymongo.Error as e:
      print(f"Error connecting to Mongo Platform: {e}")
      sys.exit(1)

def closest(list, Number):
   aux = []
   for valor in list:
      aux.append(abs(Number-valor))
   return aux.index(min(aux))

def getSongs(event):
   activity = event['activity']
   duration = event['duration']
   # song length 3.5
   # 5,9,13,17
   songLen = 3.5
   # query 4x
   options= 16
   limit = math.ceil(duration/songLen) * options
   print(limit)

   conn = get_db_connection()
   cursor = conn.cursor()
   query = ("SELECT songID, duration " +
         "FROM Song " +
         "WHERE popularity > 25 and activity LIKE '%"+ activity + "%' " + 
         "ORDER BY RAND() " +
         "LIMIT " + str(limit) + ";")
   cursor.execute(query)      
   songs = cursor.fetchall()
   print(songs)
   l={}
   for song in songs:
      l[song['songID']] = song['duration']
   return l

def createActivityPlaylist(event):
   duration = event['duration']
   songs = getSongs(event)
   durations = {15: 900000, 30: 1800000, 45: 2700000, 60:3600000}
   activityDuration= durations[duration]

   playlists = binpacking.to_constant_volume(songs, activityDuration)
   songsPerPlaylist = [list(playlist.keys()) for playlist in playlists]

   realDurations = [sum(playlist.values()) for playlist in playlists]
   print(realDurations)
   bestDuration = closest(realDurations,activityDuration)
   bestPlaylist = playlists[bestDuration]
   print(bestPlaylist)
   return bestPlaylist

def insert_playlist(scheduleID, userEmail, username, playlistSongs):
   # insert into playlist
   playlistName = username + str(datetime.date.today()) 
   dateCreated = datetime.date.today() 
   email = userEmail
   conn = get_db_connection()
   cursor = conn.cursor()
   query = "insert into Playlist(scheduleID, playlistName, dateCreated, email) values( %s, %s, %s, %s)"
   values =(scheduleID, playlistName, dateCreated, email)
   cursor.execute(query, values)
   playlistID = int(cursor.lastrowid)
   conn.commit()
   # insert into made of
   songNumber = 1
   for playlist in playlistSongs:
      for songID in playlist.keys():
         query = "insert into MadeOf(playlistID, songID, songNumber) values(%s, %s, %s)"
         values = (playlistID, songID, songNumber)
         cursor.execute(query, values)
         conn.commit()
         songNumber = songNumber + 1
   conn.close()

def generate(request_data, scheduleID, userEmail, username):
   playlistSongs = []
   for event in request_data['events']:
      print (event['activity'])
      playlistSongs.append(createActivityPlaylist(event))
   insert_playlist(scheduleID, userEmail, username, playlistSongs)

@app.route('/api/generatePlaylist', methods = ['GET','POST'])
def generatePlaylist():
   if request.method == 'POST':     
      start = time.time()
      request_data = request.get_json()
      scheduleID = (request.args.get('scheduleID'))
      userEmail = (request.args.get('userEmail'))
      username = request_data['username']
      print(scheduleID) # use to get events
      print(userEmail)
      generate(request_data, scheduleID, userEmail, username)
      end = time.time()
      print(end - start) 
      return jsonify(status='successful')
   else:
      return jsonify(status='ok')

@app.route('/api/mongotest', methods=['GET'])
def connect_mongodb_test():
   client = get_mongo_client()
   db = client["schedule"]
   mycol = db["sched"]
   test_result_list = list(mycol.find())
   print(test_result_list)
   return jsonify(test_result_list)

@app.route('/api/availableSchedules', methods=['GET'])
def mongodb_availableSchedules():
   client = get_mongo_client()
   db = client["schedule"]
   mycol = db["sched"]
   test_result_list = list(mycol.find().sort('createdDate', -1).limit(10))
   test_result_list = json.loads(json_util.dumps(test_result_list))
   print(test_result_list)
   return jsonify(test_result_list)

@app.route('/api/getUserSchedules', methods=['GET'])
def mongodb_getUserSchedules():
   if request.method == 'GET':
      user = request.args.get('user')
      client = get_mongo_client()
      db = client["schedule"]
      mycol = db["sched"]
      test_result_list = list(mycol.find({'createdBy': user}))
      test_result_list = json.loads(json_util.dumps(test_result_list))
      return jsonify(test_result_list)
   else:
      return jsonify(status='ok')

@app.route('/api/filteredSchedules', methods=['GET'])
def mongodb_filteredSchedules():
   if request.method == 'GET':
      limit = int(request.args.get('limit'))
      scheduleName = request.args.get('scheduleName')
      createdBy = request.args.get('createdBy')
      client = get_mongo_client()
      db = client["schedule"]
      mycol = db["sched"]
      test_result_list = []
      if not scheduleName:
         test_result_list = list(mycol.find({'createdBy': createdBy}).limit(limit))
      elif not createdBy:
         test_result_list = list(mycol.find({'name': {'$regex': scheduleName, '$options': 'i' }}).limit(limit))
      else:
         test_result_list = list(mycol.find({'name': {'$regex': scheduleName, '$options': 'i' }, 'createdBy': createdBy}).limit(limit))
      test_result_list = json.loads(json_util.dumps(test_result_list))
      return jsonify(test_result_list)
   else:
      return jsonify(status='ok')

@app.route('/api/addSchedule', methods=['GET','POST'])
def mongodb_addSchedule():
   if request.method == 'POST':
      request_data = request.get_json()      
      userEmail = (request.args.get('userEmail'))
      request_data["createdDate"] = datetime.datetime.now() 
      client = get_mongo_client()
      db = client["schedule"]
      mycol = db["sched"]
      scheduleID = mycol.insert(request_data)
      print(scheduleID)
      print(request_data)      
      generate(request_data, scheduleID, userEmail, request_data['createdBy'])
      return jsonify(status='successful')
   else:
      return jsonify(status='ok')

@app.route('/api/mongodeletetest', methods=['GET'])
def mongodb_delete_test():
   name = request.args.get('name')
   client = get_mongo_client()
   db = client["schedule"]
   mycol = db["sched"]
   myquery = { "name": name }
   mycol.delete_one(myquery)
   return jsonify(status='ok')

@app.route('/api/test', methods = ['GET','POST'])
def hello_world():
   return 'Hello from flask'
   if request.method == 'POST':
      return 'Hello from Flask!'

@app.route('/api/register', methods=['GET', 'POST'])
def user_profile():
   if request.method == 'POST':
      request_data = request.get_json()
      username = request_data['username']
      email = request_data['email']
      password = request_data['pwd']
      # insert into db
      conn = get_db_connection()
      cursor = conn.cursor()
      cursor.execute("select * from User where email='%s';" % email)
      result = cursor.fetchone()
      if result is None:
         query = "insert into User(email, username, password) values(%s, %s, %s)"
         values = (email, username, password)
         cursor.execute(query, values)
         conn.commit()
         print("affected rows = {}".format(cursor.rowcount))
         cursor.close()
         return jsonify(status='successful')
      else:
         return jsonify(status='failed')
   elif request.method == 'GET':
      return jsonify(status='ok')
   else:
      return jsonify(status='ok')

@app.route('/api/login', methods=['GET', 'POST'])
def user_login():
   if request.method == 'POST':
      request_data = request.get_json()
      email = request_data['email']
      password = request_data['pwd']
      conn = get_db_connection()
      cursor = conn.cursor()
      query1 = "select email from User where email=%s"
      values1 = (email)
      cursor.execute(query1, values1)
      result1 = cursor.fetchone()
      if result1 is None:
         return jsonify(status = 'Oops, email not exist')
      else:
         query = "select username from User where email=%s and password=%s"
         values =(email,password)
         cursor.execute(query, values)
         result = cursor.fetchone()
         if result is None:
            return jsonify(status='Sorry, password not match')
         else:
            username = result["username"]
            return jsonify(username=username, email= email)
   elif request.method == 'GET':
      return jsonify(status='ok')
   else:
      return jsonify(status='ok')

@app.route('/api/userupdate', methods=['POST'])
def user_update_profile():
   if request.method == 'POST':
      request_data = request.get_json()
      email = request_data['email']
      username = request_data['username']
      password = request_data['pwd']
      conn = get_db_connection()
      cursor = conn.cursor()
      query = "update User set username=%s, password=%s where email=%s;"
      values =(username,password,email)
      cursor.execute(query, values)
      conn.commit()
      return jsonify(status='successful')
   else:
      return jsonify(status='ok')

@app.route('/api/playlist/other', methods=['GET'])
def get_other_playlist():
   if request.method == 'GET':
      conn = get_db_connection()
      cursor = conn.cursor()
      cursor.execute("select * from Playlist")
      results = cursor.fetchall()
      playlists = []
      for playlist in results:
         songs = get_songs_by_playlist(playlist["playlistID"])
         playlist["songs"] = songs
         playlists.append(playlist)
      return jsonify(otherPlaylist=playlists)
   else:
      return jsonify(status='ok')

def get_songs_by_playlist(playlistID):
   conn = get_db_connection()
   cursor = conn.cursor()
  # query = "select Song.songID, songName, duration, popularity, year, artist, MadeOf.songNumber from Song join MadeOf on Song.songID=MadeOf.songID where MadeOf.playlistID=%s ORDER BY MadeOf.songNumber ASC"
   query = "SELECT * FROM viewMadeofSongs WHERE viewPlaylistID=%s"
   values =(playlistID)
   cursor.execute(query, values)
   songs = cursor.fetchall()
   for song in songs:
      song["id"] = "song-" + str(song["songID"])
   return songs

@app.route('/api/playlist/mine', methods=['POST', 'PUT'])
def myplaylist():
   if request.method == 'POST':
      
      request_data = request.get_json()
      email = request_data['email']
      keywords = request_data['keywords']
      conn = get_db_connection()
      cursor = conn.cursor()
      sql_default = ("select * from Playlist where email = '" + email + "' ORDER BY playlistID DESC")
      if keywords:
         sql_default = ("SELECT * from Playlist WHERE email = '" + email + "' AND playlistName LIKE '%" + keywords + "%' ;")
      cursor.execute(sql_default)
      results = cursor.fetchall()
      playlists = []
      for playlist in results:
         songs = get_songs_by_playlist(playlist["playlistID"])
         playlist["songs"] = songs
         playlist["displayOnPage"] = False
         playlist["id"] = str(playlist["playlistID"])
         playlists.append(playlist)
      return jsonify(myplaylist=playlists)
   elif request.method == 'PUT':
      request_data = request.get_json()
      playlistID = request_data['playlistID']
      reorderedSongs = request_data['reorderedSongs']
      activity = request_data['activity']
      actionType = request_data['actionType']
      # delete songs from madeof

      try:
         conn = get_db_connection()
         cursor1 = conn.cursor()
         #querry1 = "delete from MadeOf where playlistID={0} ".format(playlistID)
         if actionType == 'dragdrop':
             # update all songs in Madeof
             songNumber = 1
             for index, song in enumerate(reorderedSongs):
               query = "UPDATE MadeOf SET songNumber = %s  WHERE (playlistID=%s AND songID=%s)"
               values = (songNumber, playlistID, song["songID"])
               cursor1.execute(query, values)
               conn.commit()
               songNumber = songNumber + 1
         elif actionType == 'addnew':
            print('add new song')
            # only insert the last song into madeof
            songNumber = len(reorderedSongs) + 1
            lastSong = reorderedSongs[-1]
            print(lastSong)
            query = "insert into MadeOf (playlistID, songNumber, songID, activity) values(%s, %s, %s, %s)"
            values = (playlistID, songNumber, lastSong["songID"], activity)
            print(playlistID)
            cursor1.execute(query, values)
            conn.commit()
         # insert songs into madeof
         songNumber = 1
         print("numSongsPlaylist: " + str(len(reorderedSongs)))
      except pymysql.Error as e:
         # rollback changes
         conn.rollback()
         print("roll back Update playlist transaction")
         print(e)
      finally:
         cursor1.close()
         conn.close()
      return jsonify(status='ok')
   else:
      return jsonify(status='ok')

@app.route('/api/myplaylist', methods=['DELETE'])
def delete_playlist():
   if request.method == 'DELETE':
      request_data = request.get_json()
      email = request_data['email']
      playlistID = request_data['playlistID']
      try:
         conn = get_db_connection()
         cursor1 = conn.cursor()
         #begin transaction
         conn.begin()
         querry = "delete from Playlist where playlistID=%s and email=%s"
         # querry1 = "delete from MadeOf where playlistID=%s "
         print(email, playlistID)
         values = (playlistID, email)
         cursor1.execute(querry, values)
         # cursor1.execute(querry1, playlistID)
         cursor1.callproc('psDeleteMadeOf', [playlistID])
         conn.commit()
         print("delete playlist transaction committed.")
      except pymysql.Error as e:
         conn.rollback()
         print("roll back delete playlist")
         print(e)
      finally:
         cursor1.close()
         conn.close()
      return jsonify(status = 'ok')

#delete song in playlist
@app.route('/api/playlist/song', methods=['GET', 'POST', 'DELETE'])
def user_playlist_song():
   if request.method == 'DELETE':
      request_data = request.get_json()
      email = request_data['email']
      playlistID = request_data['playlistID']
      songID = request_data['songID']
      # insert into db
      conn = get_db_connection()
      cursor = conn.cursor()
      querry = "select * from Playlist where email=%s and playlistID=%s"
      values = (email, playlistID)
      cursor.execute(querry, values)
      result = cursor.fetchone()
      if result is None:
         return jsonify(status = 'Sorry, playlist not found!')
      else:
         cursor1 = conn.cursor()
         querry1 = "delete from MadeOf where playlistID={0} and songID={1} limit 1".format(playlistID, songID)
         print(querry1)
         #values1 = (playlistID, songID)
         cursor1.execute(querry1)
         conn.commit()
         conn.close()
         return jsonify(status = cursor1.rowcount)
   else:
      return jsonify(status='ok')
@app.route('/api/searchsong', methods = ['GET','POST'])
def get_song():
  conn = get_db_connection()
  cursor = conn.cursor()
  limit = int(request.args.get('limit'))
  songName=request.args.get('songName')
  songArtist=request.args.get('songArtist')
  songYear=request.args.get('songYear')
  songActivity=request.args.get('songActivity')
  query = ("SELECT Song.songID, Song.songName, Song.duration, Song.activity, Song.artist, Song.popularity, Song.year " +
            "FROM Song " +
            "WHERE songName LIKE '%"+ songName + "%' " +
            "AND artist LIKE '%"+ songArtist + "%' " +
            "AND year LIKE '%"+songYear + "%'" +
            "AND activity LIKE '%"+songActivity + "%'" +
            "LIMIT " + str(limit) + ";")
  print(query, file=sys.stderr)
  cursor.execute(query)
  songs = cursor.fetchall()
  return jsonify(songlist=songs)
#/api/recommendsong
@app.route('/api/recommendsong', methods = ['GET'])
def get_recommended_song():
   conn = get_db_connection()
   cursor = conn.cursor()
   playlistID = request.args.get('clickedPlaylistID')
   songActivity =request.args.get('songActivity')
   print("call procedure")
   cursor.callproc('psGetRecommend', [songActivity, playlistID])
   songs = cursor.fetchall()
   return jsonify(songlist=songs)

@app.route('/api/updatePlaylistName', methods=['PUT'])
def updatePlaylistName():
   if request.method == 'PUT':
      request_data = request.get_json()
      email = request_data['email']
      playlistID = request_data['playlistID']
      playlistName = request_data['playlistName']
      conn = get_db_connection()
      cursor = conn.cursor()
      query = "update Playlist set playlistName=%s where email=%s and playlistID=%s;"
      values =(playlistName, email, playlistID)
      print(values)
      cursor.execute(query, values)
      conn.commit()
      return jsonify(status='successful')
   else:
      return jsonify(status='ok')


      
@app.route('/api/getSongs', methods = ['GET'])
def getSongsCall():
   if request.method == 'GET':
      conn = get_db_connection()
      cursor = conn.cursor()
      limit = 68
      activity = "studying"
      query = ("SELECT songID, duration " +
            "FROM Song " +
            "WHERE popularity > 25 and activity LIKE '%"+ activity + "%' " + 
            "ORDER BY RAND() " +
            "LIMIT " + str(limit) + ";")
      cursor.execute(query)      
      songs = cursor.fetchall()
      print(songs)
      l={}
      for song in songs:
        l[song['songID']] = song['duration']
      print(l)
      return jsonify(songlist=songs)


if __name__ == '__main__':
   app.run()
